// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_MATRIX_VECTOR_ALGORITHM_HH
#define DUNE_MATRIX_VECTOR_ALGORITHM_HH

#include <type_traits>

#include <dune/common/hybridutilities.hh>
#include <dune/common/typetraits.hh>

namespace Dune {
namespace MatrixVector {

/**
 * \brief Hybrid for loop over sparse range
 */
template <class Range, class F,
          std::enable_if_t<Dune::IsTupleOrDerived<std::decay_t<Range>>::value, int> = 0>
void sparseRangeFor(Range&& range, F&& f) {
  using namespace Dune::Hybrid;
  forEach(integralRange(size(range)), [&](auto&& i) {
      f(range[i], i);
  });
}

/**
 * \brief Hybrid for loop over sparse range
 */
template<class Range, class F,
          std::enable_if_t<not Dune::IsTupleOrDerived<std::decay_t<Range>>::value, int> = 0>
void sparseRangeFor(Range&& range, F&& f)
{
  auto it = range.begin();
  auto end = range.end();
  for (; it != end; ++it)
    f(*it, it.index());
}



} // namespace MatrixVector
} // namespace Dune


#endif // DUNE_MATRIX_VECTOR_ALGORITHM_HH
