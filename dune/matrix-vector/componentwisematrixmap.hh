// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_COMPONENT_WISE_MATRIX_MAP_HH
#define DUNE_FUFEM_COMPONENT_WISE_MATRIX_MAP_HH

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/fufem/arithmetic.hh>
#include <dune/fufem/indexedsliceiterator.hh>
#include <dune/matrix-vector/singlenonzerocolumnmatrix.hh>
#include <dune/matrix-vector/singlenonzerorowmatrix.hh>

template<class K, int ROWS, int COLS>
class ComponentWiseMatrixMap
{
public:
    static const int rowFactor = COLS;
    static const int colFactor = 1;
    using Block = Dune::FieldMatrix<K, ROWS, 1>;
    typedef typename Dune::BCRSMatrix<Block> Matrix;
    typedef SingleNonZeroColumnMatrix<K, ROWS, COLS, const Block&> block_type;

    block_type apply(const Block& a, int row, int col, int virtualRow, int virtualCol) const
    {
        return block_type(a, virtualRow);
    }
};



template<class K, int ORIGINALROWS, int ORIGINALCOLS>
class TransposedComponentWiseMatrixMap
{
public:
    static const int rowFactor = 1;
    static const int colFactor = ORIGINALCOLS;
    using OriginalBlock = Dune::FieldMatrix<K, 1, ORIGINALROWS>;
    typedef typename Dune::BCRSMatrix<OriginalBlock> Matrix;
    typedef SingleNonZeroRowMatrix<K, ORIGINALCOLS, ORIGINALROWS> block_type; // TODO give const ref block type when support (see column case)

    block_type apply(const OriginalBlock& a, int row, int col, int virtualRow, int virtualCol) const
    {
        return block_type(a, virtualCol);
    }
};



#endif
