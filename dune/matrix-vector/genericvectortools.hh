// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH
#define DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH

/** \file
    \brief Various tools for working with istl vectors of arbitrary nesting depth
*/

#include <iostream>
#include <dune/common/fvector.hh>


/** \brief Various tools for working with istl vectors of arbitrary nesting depth
*/
namespace Dune {
namespace MatrixVector {
namespace Generic { // TODO change namespace name

template<class Vector> struct Helper;

//! Write vector to given stream
template<class Vector>
void writeBinary(std::ostream& s, const Vector& v)
{ Dune::MatrixVector::Generic::Helper<Vector>::writeBinary(s, v); }

template <class Vector>
struct Helper
{
    static void writeBinary(std::ostream& s, const Vector& v)
    {
        typename Vector::const_iterator it = v.begin();
        typename Vector::const_iterator end = v.end();
        for(; it!=end; ++it)
            Helper<typename Vector::block_type>::writeBinary(s, *it);
    }
};

template<class Field, int n>
struct Helper<Dune::FieldVector<Field,n>>
{
    static void writeBinary(std::ostream& s, const Dune::FieldVector<Field,n>& v)
    {
        typedef typename Dune::FieldVector<Field,n> Vector;
        typename Vector::const_iterator it = v.begin();
        typename Vector::const_iterator end = v.end();
        for(; it!=end; ++it)
            s.write(reinterpret_cast<const char*>(&(*it)), sizeof(Field));
    }
};



} // end namespace Generic
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH

