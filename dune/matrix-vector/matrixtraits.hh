#ifndef DUNE_MATRIX_VECTOR_MATRIXTRAITS_HH
#define DUNE_MATRIX_VECTOR_MATRIXTRAITS_HH

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

namespace Dune {
  namespace MatrixVector {

    /** \brief Class to identify matrix types and extract information
     *
     * Specialize this class for all types that can be used like a matrix.
     */
    template<class T>
    struct MatrixTraits
    {
        enum { isMatrix=false };
        enum { rows=-1};
        enum { cols=-1};
    };

    template<class T, int n, int m>
    struct MatrixTraits<Dune::FieldMatrix<T,n,m> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=m};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::DiagonalMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::ScaledIdentityMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T>
    struct MatrixTraits<Dune::BCRSMatrix<T> >
    {
        enum { isMatrix=true };
    };

    template<class... T>
    struct MatrixTraits<MultiTypeBlockMatrix<T...> >
    {
        enum { isMatrix=true };
    };
  }
}
#endif
