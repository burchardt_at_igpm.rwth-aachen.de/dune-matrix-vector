#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/indices.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/scaledidmatrix.hh>

#include "common.hh"

#include "../axpy.hh"
#include "../axy.hh"
#include "../crossproduct.hh"
#include "../transpose.hh"

using namespace Dune::MatrixVector;

template <class FT>
struct ArithmeticTestSuite {
  bool verbose;

  const FT tol = 10 * std::numeric_limits<FT>::epsilon();

  ArithmeticTestSuite(bool verbose = false) : verbose(verbose) {}

  bool check() {
    bool passed = true;

    passed = passed and checkProductHelperMethods();
    passed = passed and checkScaledProductHelperMethods();

    passed = passed and checkCrossProduct();

    passed = passed and checkAxy();
    passed = passed and checkBmAxy();

    passed = passed and checkTranspose();

    passed = passed and checkBCRSSubPattern();

    return passed;
  }

private:
  bool checkProductHelperMethods() {
    if (verbose)
      std::cout << "checkProductHelperMethods<" << Dune::className<FT>() << ">"
                << std::endl;

    bool passed = true;

    passed = passed and checkProductHelperMethods_MatrixMatrixMatrix();
    passed = passed and checkProductHelperMethods_MatrixScalarMatrix();

    passed = passed and checkProductHelperMethods_VectorMatrixVector();
    passed = passed and checkProductHelperMethods_VectorScalarVector();

    passed = passed and checkProductHelperMethods_ScalarScalarScalar();

    return passed;
  }

  bool checkScaledProductHelperMethods() {
    if (verbose)
      std::cout << "checkScaledProductHelperMethods<" << Dune::className<FT>()
                << ">" << std::endl;

    bool passed = true;

    passed = passed and checkScaledProductHelperMethods_MatrixMatrixMatrix();
    passed = passed and checkScaledProductHelperMethods_MatrixScalarMatrix();

    passed = passed and checkScaledProductHelperMethods_VectorMatrixVector();
    passed = passed and checkScaledProductHelperMethods_VectorScalarVector();

    passed = passed and checkScaledProductHelperMethods_ScalarScalarScalar();

    return passed;
  }

  bool checkCrossProduct() {
    if (verbose)
      std::cout << "checkCrossProduct<" << Dune::className<FT>() << ">"
                << std::endl;

    bool passed = true;

    typedef Dune::FieldVector<FT, 3> VectorType;
    const VectorType fieldVector_x = {1, 2, 3}, fieldVector_y = {2, 3, 1};

    passed = passed and isCloseDune(crossProduct(fieldVector_x, fieldVector_y),
                                    VectorType{-7, 5, -1});

    return passed;
  }

  bool checkAxy() {
    if (verbose)
      std::cout << "checkAxy<" << Dune::className<FT>() << ">" << std::endl;

    bool passed = true;

    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_A = {{1, 2}, {3, 4}};
    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_A = {3, 2};
    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_A(2);

    const Dune::FieldVector<FT, 2> fieldVector_x = {1, 2},
                                   fieldVector_y = {2, 3};

    passed =
        passed and
        isCloseAbs(Axy(squareFieldMatrix_A, fieldVector_x, fieldVector_y), 43);
    passed =
        passed and
        isCloseAbs(Axy(diagonalMatrix_A, fieldVector_x, fieldVector_y), 18);
    passed = passed and
             isCloseAbs(
                 Axy(scaledIdentityMatrix_A, fieldVector_x, fieldVector_y), 16);

    return passed;
  }

public:
  bool checkBmAxy() {
    if (verbose)
      std::cout << "checkBmAxy<" << Dune::className<FT>() << ">" << std::endl;

    bool passed = true;

    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_A = {{1, 2}, {3, 4}};
    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_A = {3, 2};
    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_A(2);

    const Dune::FieldVector<FT, 2> fieldVector_x = {1, 2},
                                   fieldVector_y = {2, 3},
                                   fieldVector_b = {10, 13};

    passed = passed and isCloseAbs(bmAxy(squareFieldMatrix_A, fieldVector_b,
                                         fieldVector_x, fieldVector_y),
                                   16);
    passed = passed and isCloseAbs(bmAxy(diagonalMatrix_A, fieldVector_b,
                                         fieldVector_x, fieldVector_y),
                                   41);
    passed = passed and isCloseAbs(bmAxy(scaledIdentityMatrix_A, fieldVector_b,
                                         fieldVector_x, fieldVector_y),
                                   43);

    return passed;
  }

  bool checkTranspose() {
    if (verbose)
      std::cout << "checkTranspose<" << Dune::className<FT>() << ">"
                << std::endl;

    bool passed = true;

    typedef Dune::FieldMatrix<FT, 3, 3> SquareFieldMatrix;
    typedef Dune::FieldMatrix<FT, 2, 4> RectFieldMatrix;
    typedef Dune::DiagonalMatrix<FT, 2> DiagonalMatrix;
    typedef Dune::ScaledIdentityMatrix<FT, 2> ScaledIdentityMatrix;

    typedef Dune::BCRSMatrix<Dune::FieldMatrix<FT, 2, 4>> BCRSofRectFieldMatrix;

    const SquareFieldMatrix squareFieldMatrix_A = {
        {1, 2, 3}, {3, 4, 5}, {5, 6, 7}};
    const Transposed<SquareFieldMatrix> squareFieldMatrix_check = {
        {1, 3, 5}, {2, 4, 6}, {3, 5, 7}};
    Transposed<SquareFieldMatrix> squareFieldMatrix_At(0);

    const RectFieldMatrix rectFieldMatrix_A = {{1, 2, 3, 4}, {3, 4, 5, 6}};
    const Transposed<RectFieldMatrix> rectFieldMatrix_check = {
        {1, 3}, {2, 4}, {3, 5}, {4, 6}};
    Transposed<RectFieldMatrix> rectFieldMatrix_At(0);

    const DiagonalMatrix diagonalMatrix_A = {3, 2};
    const Transposed<DiagonalMatrix> diagonalMatrix_check = {3, 2};
    Transposed<DiagonalMatrix> diagonalMatrix_At(0);

    const ScaledIdentityMatrix scaledIdentityMatrix_A(2);
    const Transposed<ScaledIdentityMatrix> scaledIdentityMatrix_check(2);
    Transposed<ScaledIdentityMatrix> scaledIdentityMatrix_At(0);

    BCRSofRectFieldMatrix bcrsRectFieldMatrix_A(2, 3,
                                                BCRSofRectFieldMatrix::random);
    bcrsRectFieldMatrix_A.setrowsize(0, 2);
    bcrsRectFieldMatrix_A.setrowsize(1, 1);
    bcrsRectFieldMatrix_A.endrowsizes();

    bcrsRectFieldMatrix_A.addindex(0, 0);
    bcrsRectFieldMatrix_A.addindex(0, 2);
    bcrsRectFieldMatrix_A.addindex(1, 0);
    bcrsRectFieldMatrix_A.endindices();

    bcrsRectFieldMatrix_A[0][0] = RectFieldMatrix{{1, 2, 3, 4}, {3, 4, 5, 6}};
    bcrsRectFieldMatrix_A[0][2] = RectFieldMatrix{{2, 2, 3, 3}, {1, 1, 4, 2}};
    bcrsRectFieldMatrix_A[1][0] = RectFieldMatrix{{3, 1, 2, 5}, {4, 3, 2, 3}};

    Transposed<BCRSofRectFieldMatrix> bcrsRectFieldMatrix_check(
        3, 2, Transposed<BCRSofRectFieldMatrix>::random);
    bcrsRectFieldMatrix_check.setrowsize(0, 2);
    bcrsRectFieldMatrix_check.setrowsize(1, 0);
    bcrsRectFieldMatrix_check.setrowsize(2, 1);
    bcrsRectFieldMatrix_check.endrowsizes();

    bcrsRectFieldMatrix_check.addindex(0, 0);
    bcrsRectFieldMatrix_check.addindex(0, 1);
    bcrsRectFieldMatrix_check.addindex(2, 0);
    bcrsRectFieldMatrix_check.endindices();

    bcrsRectFieldMatrix_check[0][0] =
        Transposed<RectFieldMatrix>{{1, 3}, {2, 4}, {3, 5}, {4, 6}};
    bcrsRectFieldMatrix_check[2][0] =
        Transposed<RectFieldMatrix>{{2, 1}, {2, 1}, {3, 4}, {3, 2}};
    bcrsRectFieldMatrix_check[0][1] =
        Transposed<RectFieldMatrix>{{3, 4}, {1, 3}, {2, 2}, {5, 3}};

    Transposed<BCRSofRectFieldMatrix> bcrsRectFieldMatrix_At;

    transpose(squareFieldMatrix_A, squareFieldMatrix_At);
    passed =
        passed and isCloseDune(squareFieldMatrix_At, squareFieldMatrix_check);

    transpose(rectFieldMatrix_A, rectFieldMatrix_At);
    passed = passed and isCloseDune(rectFieldMatrix_At, rectFieldMatrix_check);

    transpose(diagonalMatrix_A, diagonalMatrix_At);
    passed = passed and isCloseDune(diagonalMatrix_At, diagonalMatrix_check);

    transpose(scaledIdentityMatrix_A, scaledIdentityMatrix_At);
    passed = passed and
             isCloseDune(scaledIdentityMatrix_At, scaledIdentityMatrix_check);

    transpose(bcrsRectFieldMatrix_A, bcrsRectFieldMatrix_At);
    passed = passed and
             isCloseDune(bcrsRectFieldMatrix_At, bcrsRectFieldMatrix_check);

    return passed;
  }

  bool checkProductHelperMethods_MatrixMatrixMatrix() {
    bool passed = true;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_b = {{1, 2}, {3, 4}},

                                      squareFieldMatrix_c = {{2, 4}, {3, 5}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_b = {3, 2},
                                      diagonalMatrix_c = {2, 1};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_b(2),
        scaledIdentityMatrix_c(4);
    // case FM += FM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 14}, {18, 32}};

      addProduct(squareFieldMatrix_a, squareFieldMatrix_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, squareFieldMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{6, 12}, {6, 10}};

      addProduct(squareFieldMatrix_a, diagonalMatrix_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, diagonalMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += FM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{2, 2}, {6, 4}};

      addProduct(squareFieldMatrix_a, squareFieldMatrix_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, squareFieldMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{6, 0}, {0, 2}};

      addProduct(squareFieldMatrix_a, diagonalMatrix_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, diagonalMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 8}, {6, 10}};

      addProduct(squareFieldMatrix_a, scaledIdentityMatrix_b,
                 squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaledIdentityMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += FM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 8}, {12, 16}};

      addProduct(squareFieldMatrix_a, squareFieldMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, squareFieldMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 8}};

      addProduct(squareFieldMatrix_a, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{12, 0}, {0, 8}};

      addProduct(squareFieldMatrix_a, diagonalMatrix_b, scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, diagonalMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 0}, {0, 2}};

      addProduct(squareFieldMatrix_a, scaledIdentityMatrix_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaledIdentityMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += DM*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {6, 2};

      addProduct(diagonalMatrix_a, diagonalMatrix_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, diagonalMatrix_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += DM*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {12, 8};

      addProduct(diagonalMatrix_a, diagonalMatrix_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, diagonalMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += SM*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {4, 2};

      addProduct(diagonalMatrix_a, scaledIdentityMatrix_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaledIdentityMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += SM*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 8};

      addProduct(diagonalMatrix_a, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }

    // case SM += SM*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(8);

      addProduct(scaledIdentityMatrix_a, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // Extra test: SM += a*SM*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(8);

      addProduct(scaledIdentityMatrix_a, 2, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      addProduct(scaledIdentityMatrix_a, -1, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, 2, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      addProduct(scaledIdentityMatrix_check, 1, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 3, 2> rectFieldMatrix_b = {
        {1, 2}, {3, 4}, {5, 6}};

    const Dune::FieldMatrix<FT, 2, 4> rectFieldMatrix_c = {{2, 3, 2, 4},
                                                           {1, 4, 3, 5}};
    // case FM<3,4> += FM<3,2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 3, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {
              {4, 11, 8, 14}, {10, 25, 18, 32}, {16, 39, 28, 50}};

      addProduct(rectFieldMatrix_a, rectFieldMatrix_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, rectFieldMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<2,4> += DM<2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{6, 9, 6, 12}, {2, 8, 6, 10}};

      addProduct(rectFieldMatrix_a, diagonalMatrix_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, diagonalMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<3,2> += FM<3,2>*DM<2>
    {
      typedef Dune::FieldMatrix<FT, 3, 2> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{2, 2}, {6, 4}, {10, 6}};

      addProduct(rectFieldMatrix_a, rectFieldMatrix_b, diagonalMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, rectFieldMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<2,4> += SM<2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{4, 6, 4, 8}, {2, 8, 6, 10}};

      addProduct(rectFieldMatrix_a, scaledIdentityMatrix_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaledIdentityMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<3,2> += FM<3,2>*SM<2>
    {
      typedef Dune::FieldMatrix<FT, 3, 2> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{4, 8}, {12, 16}, {20, 24}};

      addProduct(rectFieldMatrix_a, rectFieldMatrix_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, rectFieldMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    return passed;
  }

  bool checkProductHelperMethods_MatrixScalarMatrix() {
    bool passed = true;

    const FT scalar_b = 2;
    const double double_b = 2.0;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_c = {{2, 4}, {3, 5}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_c = {2, 1};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_c(4);

    // case FM += scalar*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 8}, {6, 10}};

      addProduct(squareFieldMatrix_a, scalar_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scalar_b, squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += scalar*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 0}, {0, 2}};

      addProduct(squareFieldMatrix_a, scalar_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scalar_b, diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += scalar*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 8}};

      addProduct(squareFieldMatrix_a, scalar_b, scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scalar_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += scalar*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {4, 2};

      addProduct(diagonalMatrix_a, scalar_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scalar_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += scalar*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 8};

      addProduct(diagonalMatrix_a, scalar_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scalar_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case SM += scalar*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(8);

      addProduct(scaledIdentityMatrix_a, scalar_b, scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, scalar_b,
                      scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }
    // case MM += scalar*MM
    {
        using ResultType = Dune::MultiTypeBlockMatrix<
            Dune::MultiTypeBlockVector<
                Dune::FieldMatrix<FT, 1, 1>, Dune::FieldMatrix<FT, 1, 2> >,
        Dune::MultiTypeBlockVector<
                Dune::FieldMatrix<FT, 2, 1>, Dune::FieldMatrix<FT, 2, 2> > >;
        ResultType multiTypeBlockMatrix_a, multiTypeBlockMatrix_b;
        using namespace Dune::Indices;
        multiTypeBlockMatrix_a[_0][_0] = {{1}};
        multiTypeBlockMatrix_a[_0][_1] = {{2, 3}};
        multiTypeBlockMatrix_a[_1][_0] = {{4}, {5}};
        multiTypeBlockMatrix_a[_1][_1] = {{6, 7}, {8, 9}};
        multiTypeBlockMatrix_b[_0][_0] = {{9}};
        multiTypeBlockMatrix_b[_0][_1] = {{8, 7}};
        multiTypeBlockMatrix_b[_1][_0] = {{6}, {5}};
        multiTypeBlockMatrix_b[_1][_1] = {{4, 3}, {2, 1}};
        addProduct(multiTypeBlockMatrix_a, scalar_b, multiTypeBlockMatrix_b);
        ResultType multiTypeBlockMatrix_check;
        multiTypeBlockMatrix_check[_0][_0] = {{19}};
        multiTypeBlockMatrix_check[_0][_1] = {{18, 17}};
        multiTypeBlockMatrix_check[_1][_0] = {{16}, {15}};
        multiTypeBlockMatrix_check[_1][_1] = {{14, 13}, {12, 11}};
        using namespace Dune::Hybrid;
        forEach(integralRange(size(multiTypeBlockMatrix_a)), [&](auto&& i) {
            forEach(integralRange(size(multiTypeBlockMatrix_a[i])), [&](auto && j) {
                passed = passed and isCloseDune(multiTypeBlockMatrix_a[i][j],
                                                multiTypeBlockMatrix_check[i][j]);
            });
        });
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 2, 4> rectFieldMatrix_c = {{2, 3, 2, 4},
                                                           {1, 4, 3, 5}};
    // case FM<2,4> += scalar*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{4, 6, 4, 8}, {2, 8, 6, 10}};

      addProduct(rectFieldMatrix_a, scalar_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scalar_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }

    // and again for fixed scalar type double
    // SQUARE MATRICES
    // case FM += double*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 8}, {6, 10}};

      addProduct(squareFieldMatrix_a, double_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, double_b, squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += double*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 0}, {0, 2}};

      addProduct(squareFieldMatrix_a, double_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, double_b, diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += double*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 8}};

      addProduct(squareFieldMatrix_a, double_b, scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, double_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += double*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {4, 2};

      addProduct(diagonalMatrix_a, double_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, double_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += double*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 8};

      addProduct(diagonalMatrix_a, double_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, double_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case SM += double*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(8);

      addProduct(scaledIdentityMatrix_a, double_b, scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, double_b,
                      scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // RECTANGULAR MATRICES
    // case FM<2,4> += double*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{4, 6, 4, 8}, {2, 8, 6, 10}};

      addProduct(rectFieldMatrix_a, double_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, double_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }

    // BLOCKED MATRICES of RECTANGULAR MATRICES
    {
      typedef Dune::FieldMatrix<FT, 2, 4> RectFieldMatrix;
      typedef Dune::BCRSMatrix<RectFieldMatrix> BCRSofRectFieldMatrix;

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_c(
          2, 3, BCRSofRectFieldMatrix::random);
      bcrsRectFieldMatrix_c.setrowsize(0, 2);
      bcrsRectFieldMatrix_c.setrowsize(1, 1);
      bcrsRectFieldMatrix_c.endrowsizes();

      bcrsRectFieldMatrix_c.addindex(0, 0);
      bcrsRectFieldMatrix_c.addindex(0, 2);
      bcrsRectFieldMatrix_c.addindex(1, 0);
      bcrsRectFieldMatrix_c.endindices();

      bcrsRectFieldMatrix_c[0][0] = RectFieldMatrix{{1, 2, 3, 4}, {3, 4, 5, 6}};
      bcrsRectFieldMatrix_c[0][2] = RectFieldMatrix{{2, 2, 3, 3}, {1, 1, 4, 2}};
      bcrsRectFieldMatrix_c[1][0] = RectFieldMatrix{{3, 1, 2, 5}, {4, 3, 2, 3}};

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_check(bcrsRectFieldMatrix_c);
      bcrsRectFieldMatrix_check *= scalar_b;

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_a(bcrsRectFieldMatrix_c),
          bcrsRectFieldMatrix_zero(bcrsRectFieldMatrix_c);
      bcrsRectFieldMatrix_a = bcrsRectFieldMatrix_zero = 0;

      addProduct(bcrsRectFieldMatrix_a, scalar_b, bcrsRectFieldMatrix_c);
      passed = passed and
               isCloseDune(bcrsRectFieldMatrix_check, bcrsRectFieldMatrix_a);

      subtractProduct(bcrsRectFieldMatrix_check, scalar_b,
                      bcrsRectFieldMatrix_c);
      passed = passed and
               isCloseDune(bcrsRectFieldMatrix_check, bcrsRectFieldMatrix_zero);
    }

    // BLOCKED MATRICES of DIAGONAL MATRICES
    {
      typedef Dune::DiagonalMatrix<FT, 3> DiagMatrix;
      typedef Dune::BCRSMatrix<DiagMatrix> BCRSofDiagonalMatrix;

      BCRSofDiagonalMatrix bcrsDiagonalMatrix_c(2, 3,
                                                BCRSofDiagonalMatrix::random);
      bcrsDiagonalMatrix_c.setrowsize(0, 2);
      bcrsDiagonalMatrix_c.setrowsize(1, 1);
      bcrsDiagonalMatrix_c.endrowsizes();

      bcrsDiagonalMatrix_c.addindex(0, 0);
      bcrsDiagonalMatrix_c.addindex(0, 2);
      bcrsDiagonalMatrix_c.addindex(1, 0);
      bcrsDiagonalMatrix_c.endindices();

      bcrsDiagonalMatrix_c[0][0] = DiagMatrix{1, 4, 5};
      bcrsDiagonalMatrix_c[0][2] = DiagMatrix{.1, .4, .5};
      bcrsDiagonalMatrix_c[1][0] = DiagMatrix{-1, -.1, 99};

      BCRSofDiagonalMatrix bcrsDiagonalMatrix_check(bcrsDiagonalMatrix_c);
      bcrsDiagonalMatrix_check *= scalar_b;

      BCRSofDiagonalMatrix bcrsDiagonalMatrix_a(bcrsDiagonalMatrix_c);
      bcrsDiagonalMatrix_a = 0;

      addProduct(bcrsDiagonalMatrix_a, scalar_b, bcrsDiagonalMatrix_c);
      passed = passed and
               isCloseDune(bcrsDiagonalMatrix_check, bcrsDiagonalMatrix_a);

      subtractProduct(bcrsDiagonalMatrix_a, scalar_b, bcrsDiagonalMatrix_c);
      addProduct(bcrsDiagonalMatrix_a, scalar_b, bcrsDiagonalMatrix_c);
      passed = passed and
               isCloseDune(bcrsDiagonalMatrix_check, bcrsDiagonalMatrix_a);

      addProduct(bcrsDiagonalMatrix_a, 2.0, scalar_b, bcrsDiagonalMatrix_c);
      subtractProduct(bcrsDiagonalMatrix_a, scalar_b, bcrsDiagonalMatrix_c);
      subtractProduct(bcrsDiagonalMatrix_a, scalar_b, bcrsDiagonalMatrix_c);
      passed = passed and
               isCloseDune(bcrsDiagonalMatrix_check, bcrsDiagonalMatrix_a);

      addProduct(bcrsDiagonalMatrix_a, 2.0, scalar_b, bcrsDiagonalMatrix_c);
      subtractProduct(bcrsDiagonalMatrix_a, 2.0, scalar_b,
                      bcrsDiagonalMatrix_c);
      passed = passed and
               isCloseDune(bcrsDiagonalMatrix_check, bcrsDiagonalMatrix_a);
    }

    // BLOCKED MATRICES of ARBITRARY MATRIX but with differing index sets - what
    // do we expect?!

    return passed;
  }

  bool checkProductHelperMethods_VectorMatrixVector() {
    bool passed = true;

    const Dune::FieldVector<FT, 2> fieldVector_c = {4, 2};

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_b = {{1, 2}, {3, 4}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_b = {3, 2};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_b(2);

    {
      // case FV += FM*FV
      typedef Dune::FieldVector<FT, 2> ResultType;
      {
        ResultType fieldVector_a(0), fieldVector_check = {8, 20};

        addProduct(fieldVector_a, squareFieldMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, squareFieldMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
      // case FV += DM*FV
      {
        ResultType fieldVector_a(0), fieldVector_check = {12, 4};

        addProduct(fieldVector_a, diagonalMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, diagonalMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
      // case FV += SM*FV
      {
        ResultType fieldVector_a(0), fieldVector_check = {8, 4};

        addProduct(fieldVector_a, scaledIdentityMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, scaledIdentityMatrix_b,
                        fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 3, 2> rectFieldMatrix_b = {
        {1, 2}, {3, 4}, {5, 6}};

    // case FV<3> += FM<3,2>*FV<2>
    {
      Dune::FieldVector<FT, 3> fieldVector_a(0),
          fieldVector_check = {8, 20, 32};

      addProduct(fieldVector_a, rectFieldMatrix_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, rectFieldMatrix_b, fieldVector_c);
      passed = passed and
               isCloseDune(fieldVector_check, Dune::FieldVector<FT, 3>(0));
    }

    return passed;
  }

  bool checkProductHelperMethods_VectorScalarVector() {
    bool passed = true;

    const FT scalar_b = 2;
    const double double_b = 2;

    const Dune::FieldVector<FT, 2> fieldVector_c = {4, 2};

    // SQUARE MATRICES
    // case FV += scalar*FV
    {
      typedef Dune::FieldVector<FT, 2> ResultType;
      ResultType fieldVector_a(0), fieldVector_check = {8, 4};

      addProduct(fieldVector_a, scalar_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, scalar_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_check, ResultType(0));
    }
    // case FV += double*FV
    {
      typedef Dune::FieldVector<FT, 2> ResultType;
      ResultType fieldVector_a(0), fieldVector_check = {8, 4};

      addProduct(fieldVector_a, double_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, double_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_check, ResultType(0));
    }

    return passed;
  }

  bool checkProductHelperMethods_ScalarScalarScalar() {
    bool passed = true;

    const FT scalar_b = 2, scalar_c = 3;

    const double double_b = 2.0, double_c = 3.0;

    // SQUARE MATRICES
    // case scalar += scalar*scalar
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 6;

      addProduct(scalar_a, scalar_b, scalar_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scalar_b, scalar_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += double*scalar
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 6;

      addProduct(scalar_a, double_b, scalar_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, double_b, scalar_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += scalar*double
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 6;

      addProduct(scalar_a, scalar_b, double_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scalar_b, double_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += double*double
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 6;

      addProduct(scalar_a, double_b, double_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, double_b, double_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case double += double*double
    {
      typedef double ResultType;
      ResultType double_a(0), double_check = 6;

      addProduct(double_a, double_b, double_c);
      passed = passed and isCloseAbs(double_a, double_check);

      subtractProduct(double_check, double_b, double_c);
      passed = passed and isCloseAbs(double_check, ResultType(0));
    }

    return passed;
  }

  bool checkScaledProductHelperMethods_MatrixMatrixMatrix() {
    bool passed = true;

    const FT scaling = 2;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_b = {{1, 2}, {3, 4}},

                                      squareFieldMatrix_c = {{2, 4}, {3, 5}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_b = {3, 2},
                                      diagonalMatrix_c = {2, 1};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_b(2),
        scaledIdentityMatrix_c(4);
    // case FM += FM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{16, 28}, {36, 64}};

      addProduct(squareFieldMatrix_a, scaling, squareFieldMatrix_b,
                 squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, squareFieldMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{12, 24}, {12, 20}};

      addProduct(squareFieldMatrix_a, scaling, diagonalMatrix_b,
                 squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, diagonalMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += FM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{4, 4}, {12, 8}};

      addProduct(squareFieldMatrix_a, scaling, squareFieldMatrix_b,
                 diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, squareFieldMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{12, 0}, {0, 4}};

      addProduct(squareFieldMatrix_a, scaling, diagonalMatrix_b,
                 diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, diagonalMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 16}, {12, 20}};

      addProduct(squareFieldMatrix_a, scaling, scaledIdentityMatrix_b,
                 squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scaledIdentityMatrix_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += FM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 16}, {24, 32}};

      addProduct(squareFieldMatrix_a, scaling, squareFieldMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, squareFieldMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{16, 0}, {0, 16}};

      addProduct(squareFieldMatrix_a, scaling, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += DM*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{24, 0}, {0, 16}};

      addProduct(squareFieldMatrix_a, scaling, diagonalMatrix_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, diagonalMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += SM*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 4}};

      addProduct(squareFieldMatrix_a, scaling, scaledIdentityMatrix_b,
                 diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scaledIdentityMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += DM*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {12, 4};

      addProduct(diagonalMatrix_a, scaling, diagonalMatrix_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, diagonalMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += DM*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {24, 16};

      addProduct(diagonalMatrix_a, scaling, diagonalMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, diagonalMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += SM*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 4};

      addProduct(diagonalMatrix_a, scaling, scaledIdentityMatrix_b,
                 diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, scaledIdentityMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += SM*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {16, 16};

      addProduct(diagonalMatrix_a, scaling, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, scaledIdentityMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case SM += SM*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(16);

      addProduct(scaledIdentityMatrix_a, scaling, scaledIdentityMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, scaling,
                      scaledIdentityMatrix_b, scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 3, 2> rectFieldMatrix_b = {
        {1, 2}, {3, 4}, {5, 6}};

    const Dune::FieldMatrix<FT, 2, 4> rectFieldMatrix_c = {{2, 3, 2, 4},
                                                           {1, 4, 3, 5}};
    // case FM<3,4> += FM<3,2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 3, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {
              {8, 22, 16, 28}, {20, 50, 36, 64}, {32, 78, 56, 100}};

      addProduct(rectFieldMatrix_a, scaling, rectFieldMatrix_b,
                 rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, rectFieldMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<2,4> += DM<2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{12, 18, 12, 24}, {4, 16, 12, 20}};

      addProduct(rectFieldMatrix_a, scaling, diagonalMatrix_b,
                 rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, diagonalMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<3,2> += FM<3,2>*DM<2>
    {
      typedef Dune::FieldMatrix<FT, 3, 2> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{4, 4}, {12, 8}, {20, 12}};

      addProduct(rectFieldMatrix_a, scaling, rectFieldMatrix_b,
                 diagonalMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, rectFieldMatrix_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<2,4> += SM<2>*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{8, 12, 8, 16}, {4, 16, 12, 20}};

      addProduct(rectFieldMatrix_a, scaling, scaledIdentityMatrix_b,
                 rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, scaledIdentityMatrix_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    // case FM<3,2> += FM<3,2>*SM<2>
    {
      typedef Dune::FieldMatrix<FT, 3, 2> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{8, 16}, {24, 32}, {40, 48}};

      addProduct(rectFieldMatrix_a, scaling, rectFieldMatrix_b,
                 scaledIdentityMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, rectFieldMatrix_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }
    return passed;
  }

  bool checkScaledProductHelperMethods_MatrixScalarMatrix() {
    bool passed = true;

    const FT scalar_b = 2;
    const double double_b = 2.0;
    const FT scaling = 2;

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_c = {{2, 4}, {3, 5}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_c = {2, 1};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_c(4);

    // case FM += scalar*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 16}, {12, 20}};

      addProduct(squareFieldMatrix_a, scaling, scalar_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scalar_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += scalar*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 4}};

      addProduct(squareFieldMatrix_a, scaling, scalar_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scalar_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += scalar*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{16, 0}, {0, 16}};

      addProduct(squareFieldMatrix_a, scaling, scalar_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, scalar_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += scalar*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 4};

      addProduct(diagonalMatrix_a, scaling, scalar_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, scalar_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += scalar*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {16, 16};

      addProduct(diagonalMatrix_a, scaling, scalar_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, scalar_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case SM += scalar*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(16);

      addProduct(scaledIdentityMatrix_a, scaling, scalar_b,
                 scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, scaling, scalar_b,
                      scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 2, 4> rectFieldMatrix_c = {{2, 3, 2, 4},
                                                           {1, 4, 3, 5}};
    // case FM<2,4> += scalar*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{8, 12, 8, 16}, {4, 16, 12, 20}};

      addProduct(rectFieldMatrix_a, scaling, scalar_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, scalar_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }

    // and again for fixed scalar type double
    // SQUARE MATRICES
    // case FM += double*FM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 16}, {12, 20}};

      addProduct(squareFieldMatrix_a, scaling, double_b, squareFieldMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, double_b,
                      squareFieldMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += double*DM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{8, 0}, {0, 4}};

      addProduct(squareFieldMatrix_a, scaling, double_b, diagonalMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, double_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case FM += double*SM
    {
      typedef Dune::FieldMatrix<FT, 2, 2> ResultType;
      ResultType squareFieldMatrix_a(0),
          squareFieldMatrix_check = {{16, 0}, {0, 16}};

      addProduct(squareFieldMatrix_a, scaling, double_b,
                 scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(squareFieldMatrix_a, squareFieldMatrix_check);

      subtractProduct(squareFieldMatrix_check, scaling, double_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(squareFieldMatrix_check, ResultType(0));
    }
    // case DM += double*DM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {8, 4};

      addProduct(diagonalMatrix_a, scaling, double_b, diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, double_b,
                      diagonalMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case DM += double*SM
    {
      typedef Dune::DiagonalMatrix<FT, 2> ResultType;
      ResultType diagonalMatrix_a(0), diagonalMatrix_check = {16, 16};

      addProduct(diagonalMatrix_a, scaling, double_b, scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_a, diagonalMatrix_check);

      subtractProduct(diagonalMatrix_check, scaling, double_b,
                      scaledIdentityMatrix_c);
      passed = passed and isCloseDune(diagonalMatrix_check, ResultType(0));
    }
    // case SM += double*SM
    {
      typedef Dune::ScaledIdentityMatrix<FT, 2> ResultType;
      ResultType scaledIdentityMatrix_a(0), scaledIdentityMatrix_check(16);

      addProduct(scaledIdentityMatrix_a, scaling, double_b,
                 scaledIdentityMatrix_c);
      passed = passed and
               isCloseDune(scaledIdentityMatrix_a, scaledIdentityMatrix_check);

      subtractProduct(scaledIdentityMatrix_check, scaling, double_b,
                      scaledIdentityMatrix_c);
      passed =
          passed and isCloseDune(scaledIdentityMatrix_check, ResultType(0));
    }

    // RECTANGULAR MATRICES
    // case FM<2,4> += double*FM<2,4>
    {
      typedef Dune::FieldMatrix<FT, 2, 4> ResultType;
      ResultType rectFieldMatrix_a(0),
          rectFieldMatrix_check = {{8, 12, 8, 16}, {4, 16, 12, 20}};

      addProduct(rectFieldMatrix_a, scaling, double_b, rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_a, rectFieldMatrix_check);

      subtractProduct(rectFieldMatrix_check, scaling, double_b,
                      rectFieldMatrix_c);
      passed = passed and isCloseDune(rectFieldMatrix_check, ResultType(0));
    }

    // BLOCKED MATRICES
    {
      typedef Dune::FieldMatrix<FT, 2, 4> RectFieldMatrix;
      typedef Dune::BCRSMatrix<RectFieldMatrix> BCRSofRectFieldMatrix;

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_c(
          2, 3, BCRSofRectFieldMatrix::random);
      bcrsRectFieldMatrix_c.setrowsize(0, 2);
      bcrsRectFieldMatrix_c.setrowsize(1, 1);
      bcrsRectFieldMatrix_c.endrowsizes();

      bcrsRectFieldMatrix_c.addindex(0, 0);
      bcrsRectFieldMatrix_c.addindex(0, 2);
      bcrsRectFieldMatrix_c.addindex(1, 0);
      bcrsRectFieldMatrix_c.endindices();

      bcrsRectFieldMatrix_c[0][0] = RectFieldMatrix{{1, 2, 3, 4}, {3, 4, 5, 6}};
      bcrsRectFieldMatrix_c[0][2] = RectFieldMatrix{{2, 2, 3, 3}, {1, 1, 4, 2}};
      bcrsRectFieldMatrix_c[1][0] = RectFieldMatrix{{3, 1, 2, 5}, {4, 3, 2, 3}};

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_check(bcrsRectFieldMatrix_c);
      bcrsRectFieldMatrix_check *= scaling * scalar_b;

      BCRSofRectFieldMatrix bcrsRectFieldMatrix_a(bcrsRectFieldMatrix_c),
          bcrsRectFieldMatrix_zero(bcrsRectFieldMatrix_c);
      bcrsRectFieldMatrix_a = bcrsRectFieldMatrix_zero = 0;

      addProduct(bcrsRectFieldMatrix_a, scaling, scalar_b,
                 bcrsRectFieldMatrix_c);
      passed = passed and
               isCloseDune(bcrsRectFieldMatrix_check, bcrsRectFieldMatrix_a);

      subtractProduct(bcrsRectFieldMatrix_check, scaling, scalar_b,
                      bcrsRectFieldMatrix_c);
      passed = passed and
               isCloseDune(bcrsRectFieldMatrix_check, bcrsRectFieldMatrix_zero);
    }
    return passed;
  }

  bool checkScaledProductHelperMethods_VectorMatrixVector() {
    bool passed = true;

    const FT scaling = 2;

    const Dune::FieldVector<FT, 2> fieldVector_c = {4, 2};

    // SQUARE MATRICES
    const Dune::FieldMatrix<FT, 2, 2> squareFieldMatrix_b = {{1, 2}, {3, 4}};

    const Dune::DiagonalMatrix<FT, 2> diagonalMatrix_b = {3, 2};

    const Dune::ScaledIdentityMatrix<FT, 2> scaledIdentityMatrix_b(2);

    {
      // case FV += FM*FV
      typedef Dune::FieldVector<FT, 2> ResultType;
      {
        ResultType fieldVector_a(0), fieldVector_check = {16, 40};

        addProduct(fieldVector_a, scaling, squareFieldMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, scaling, squareFieldMatrix_b,
                        fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
      // case FV += DM*FV
      {
        ResultType fieldVector_a(0), fieldVector_check = {24, 8};

        addProduct(fieldVector_a, scaling, diagonalMatrix_b, fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, scaling, diagonalMatrix_b,
                        fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
      // case FV += SM*FV
      {
        ResultType fieldVector_a(0), fieldVector_check = {16, 8};

        addProduct(fieldVector_a, scaling, scaledIdentityMatrix_b,
                   fieldVector_c);
        passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

        subtractProduct(fieldVector_check, scaling, scaledIdentityMatrix_b,
                        fieldVector_c);
        passed = passed and isCloseDune(fieldVector_check, ResultType(0));
      }
    }

    // RECTANGULAR MATRICES

    const Dune::FieldMatrix<FT, 3, 2> rectFieldMatrix_b = {
        {1, 2}, {3, 4}, {5, 6}};

    // case FV<3> += FM<3,2>*FV<2>
    {
      Dune::FieldVector<FT, 3> fieldVector_a(0),
          fieldVector_check = {16, 40, 64};

      addProduct(fieldVector_a, scaling, rectFieldMatrix_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, scaling, rectFieldMatrix_b,
                      fieldVector_c);
      passed = passed and
               isCloseDune(fieldVector_check, Dune::FieldVector<FT, 3>(0));
    }

    return passed;
  }

  bool checkScaledProductHelperMethods_VectorScalarVector() {
    bool passed = true;

    const FT scalar_b = 2;
    const double double_b = 2;
    const FT scaling = 2;

    const Dune::FieldVector<FT, 2> fieldVector_c = {4, 2};

    // SQUARE MATRICES
    // case FV += scalar*FV
    {
      typedef Dune::FieldVector<FT, 2> ResultType;
      ResultType fieldVector_a(0), fieldVector_check = {16, 8};

      addProduct(fieldVector_a, scaling, scalar_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, scaling, scalar_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_check, ResultType(0));
    }
    // case FV += double*FV
    {
      typedef Dune::FieldVector<FT, 2> ResultType;
      ResultType fieldVector_a(0), fieldVector_check = {16, 8};

      addProduct(fieldVector_a, scaling, double_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_a, fieldVector_check);

      subtractProduct(fieldVector_check, scaling, double_b, fieldVector_c);
      passed = passed and isCloseDune(fieldVector_check, ResultType(0));
    }

    return passed;
  }

  bool checkScaledProductHelperMethods_ScalarScalarScalar() {
    bool passed = true;

    const FT scalar_b = 2, scalar_c = 3;

    const double double_b = 2.0, double_c = 3.0;

    const FT scaling = 2;

    // SQUARE MATRICES
    // case scalar += scalar*scalar
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 12;

      addProduct(scalar_a, scaling, scalar_b, scalar_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scaling, scalar_b, scalar_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += double*scalar
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 12;

      addProduct(scalar_a, scaling, double_b, scalar_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scaling, double_b, scalar_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += scalar*double
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 12;

      addProduct(scalar_a, scaling, scalar_b, double_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scaling, scalar_b, double_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case scalar += double*double
    {
      typedef FT ResultType;
      ResultType scalar_a(0), scalar_check = 12;

      addProduct(scalar_a, scaling, double_b, double_c);
      passed = passed and isCloseAbs(scalar_a, scalar_check);

      subtractProduct(scalar_check, scaling, double_b, double_c);
      passed = passed and isCloseAbs(scalar_check, ResultType(0));
    }
    // case double += double*double
    {
      typedef double ResultType;
      ResultType double_a(0), double_check = 12;

      addProduct(double_a, scaling, double_b, double_c);
      passed = passed and isCloseAbs(double_a, double_check);

      subtractProduct(double_check, scaling, double_b, double_c);
      passed = passed and isCloseAbs(double_check, ResultType(0));
    }

    return passed;
  }

  bool checkBCRSSubPattern() {
    typedef Dune::FieldMatrix<FT, 1, 1> M;

    Dune::BCRSMatrix<M> B(3, 3, Dune::BCRSMatrix<M>::random);
    {
      B.setrowsize(0, 3);
      B.setrowsize(1, 2);
      B.setrowsize(2, 1);
      B.endrowsizes();

      B.addindex(0, 0);
      B.addindex(0, 1);
      B.addindex(0, 2);
      B.addindex(1, 1);
      B.addindex(1, 2);
      B.addindex(2, 2);
      B.endindices();

      B[0][0] = 10;
      B[0][1] = 20;
      B[0][2] = 30;
      B[1][1] = 4000;
      B[1][2] = 5000;
      B[2][2] = 60;
    }

    Dune::BCRSMatrix<M> A(3, 3, Dune::BCRSMatrix<M>::random);
    {
      A.setrowsize(0, 3);
      A.setrowsize(1, 0);
      A.setrowsize(2, 1);
      A.endrowsizes();

      A.addindex(0, 0);
      A.addindex(0, 1);
      A.addindex(0, 2);
      A.addindex(2, 2);
      A.endindices();

      A[0][0] = B[0][0];
      A[0][1] = B[0][1];
      A[0][2] = B[0][2];
      A[2][2] = B[2][2];
    }

    FT b(2);
    Dune::BCRSMatrix<M> C(3, 3, Dune::BCRSMatrix<M>::random);
    {
      C.setrowsize(0, 3);
      C.setrowsize(1, 2);
      C.setrowsize(2, 1);
      C.endrowsizes();

      C.addindex(0, 0);
      C.addindex(0, 1);
      C.addindex(0, 2);
      C.addindex(1, 1);
      C.addindex(1, 2);
      C.addindex(2, 2);
      C.endindices();

      C[0][0] = (b + 1) * B[0][0];
      C[0][1] = (b + 1) * B[0][1];
      C[0][2] = (b + 1) * B[0][2];
      C[1][1] = B[1][1];
      C[1][2] = B[1][2];
      C[2][2] = (b + 1) * B[2][2];
    }

    addProduct(B, b, A);

    return isCloseDune(B, C);
  }
};

int main(int argc, char* argv[]) {
  Dune::MPIHelper::instance(argc, argv);
  bool passed = true;
  {
    ArithmeticTestSuite<double> testSuite;
    passed &= testSuite.check();
  }
  {
    ArithmeticTestSuite<long double> testSuite;
    passed &= testSuite.check();
  }
  {
    ArithmeticTestSuite<float> testSuite;
    passed &= testSuite.check();
  }
  return passed ? 0 : 1;
}
