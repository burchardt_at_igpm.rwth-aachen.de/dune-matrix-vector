#ifndef DUNE_MATRIX_VECTOR_TEST_COMMON_HH
#define DUNE_MATRIX_VECTOR_TEST_COMMON_HH

template <typename FT>
struct ToleranceTraits {
  static constexpr FT tol = 50 * std::numeric_limits<FT>::epsilon();
};

template <class T>
typename Dune::FieldTraits<typename T::field_type>::real_type diffDune(
    T t1, const T& t2) {
  t1 -= t2;
  return t1.infinity_norm();
}
template <class TX>
bool isCloseDune(TX t1, const TX& t2) {
  return diffDune(t1, t2) < ToleranceTraits<typename TX::field_type>::tol;
}
template <class FT, class Other>
bool isCloseAbs(FT t1, Other t2) {
  return std::abs<FT>(t1 - t2) < ToleranceTraits<FT>::tol;
}

#endif
